@extends('layouts.app')

@section('title', 'Навыки - Статус')

@section('page_js')
    <script src="{{asset('js/vue' . (env('APP_ENV') !== 'local' ? '.min' : '') . '.js' )}}"></script>
    <script src="{{asset('js/skills.js')}}"></script>
@endsection

@section('content')

    @verbatim
        <div id="app">
            <div class="container">

                <div class="row page-header text-center">
                    <div class="col-sm-12">
                        <h2>Навыки - Статус</h2>
                    </div>
                </div>

                <div class="row">

                    <div>
                        <h4> Мой статус </h4>
                        <select v-model="selected" v-on:change="changeUserStatus">
                            <option v-for="option in options" v-bind:value="option.value">
                                {{ option.text }}
                            </option>
                        </select>
                    </div>
                    <br>
                    <div>
                        <h4> Мои проекты </h4>
                        <ol>
                            <li v-for="project in allProjects" v-if="myProjects.includes(project.id)">{{project.name}}
                                <i v-if="project" v-on:click="deleteProjectForUser(project.id)"
                                   title="Удалить" class="fa fa-window-close cur-pointer font-red" aria-hidden="true"></i>
                            </li>
                        </ol>
                        <div class="container">
                            <!-- Trigger the modal with a button -->
                            <a  data-toggle="modal" data-target="#myModal">добавить+</a>

                            <!-- Modal -->
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Все активные проекты</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="add_projects_for_users">
                                                <ol>
                                                    <li v-for="project in allProjects" v-if="!myProjects.includes(project.id)">
                                                        <input type="checkbox"
                                                               v-bind:id="project.id"
                                                               v-bind:value="project.id"
                                                               v-model="checkedProjects">
                                                        <label v-bind:for="project.id">{{project.name}} </label>
                                                    </li>
                                                </ol>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success" data-dismiss="modal"
                                                    v-on:click="addProjectsForUser">Save</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <br>
                    <div id="div_in_line">
                        <h4> Мои навыки </h4>

                    </div>
                    </div>

            </div>
        </div>
    @endverbatim

@endsection

