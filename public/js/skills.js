;(function ($, Vue, G,) {

    let stats;
    let app = new Vue({
        el: '#app',
        data: {
            allProjects: G.allProjects,
            myProjects: G.myProjects,
            checkedProjects: [],
            selected: G.myStatus,
            user: G.user,
            options: [
                { text: 'Idle', value: 'Idle' },
                { text: 'Part time occupied', value: 'Part time occupied' },
                { text: 'Full time occupied', value: 'Full time occupied' }
            ]
        },
        methods: {
            addProjectsForUser: function(event) {
                var that = this;

                $.ajax({
                    url: '/user-projects/change',
                    method: 'POST',
                    data: {checkedProjects: this.checkedProjects, user_id: G.user.id}
                }).done(function(resp) {
                    if (resp.data) {
                        that.myProjects = resp.data;
                        that.checkedProjects = [];
                        if(resp.is_successful === false) {
                            $.notify({
                                message: resp.messages,
                            },{
                                type: 'danger'
                            });
                        }
                    }
                })
            },
            deleteProjectForUser: function(id) {
                var that = this;

                $.ajax({
                    url: '/user-projects/delete',
                    method: 'DELETE',
                    data: {projectId: id, user_id: G.user.id}
                }).done(function(resp) {
                    if (resp.data) {
                        that.myProjects = resp.data;
                        that.checkedProjects = [];
                        if(resp.is_successful === false) {
                            $.notify({
                                message: resp.messages,
                            },{
                                type: 'danger'
                            });
                        }
                    }
                })
            },
            changeUserStatus: function(status) {
                var that = this;

                $.ajax({
                    url: '/user-status/status',
                    method: 'POST',
                    data: {value: this.selected, user_id: G.user.id}
                }).done(function(resp) {
                    if (resp) {
                        that.selected = resp.data;
                        if(resp.is_successful === false) {
                            $.notify({
                                message: resp.messages,
                            },{
                                type: 'danger'
                            });
                        }
                    }
                })
            }
        },
    });

})(jQuery, Vue, window._globals || {});