<?php
namespace App\Helpers;

use App\Helpers\Contracts\ResponseInterface;

abstract class AbstractResponse implements ResponseInterface
{
    /**
     * @var bool
     */
    protected $isSuccessful;

    /**
     * @var array
     */
    protected $messages = [];

    /**
     * @var mixed
     */
    protected $data;

    /**
     * @return bool
     */
    public function isSuccessful(): bool
    {
        return $this->isSuccessful;
    }

    /**
     * @param int $code
     * @return $this|mixed
     */
    public function setCode(int $code)
    {
        if (!in_array($code, [static::FAILURE, static::SUCCESS])) {
            throw new \InvalidArgumentException('Error: unknown response code is passed.');
        }

        $this->isSuccessful = $code;
        return $this;
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * @param string $message
     * @return $this|mixed
     */
    public function addMessage(string $message)
    {
        $this->messages[] = $message;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param $data
     * @return $this|mixed
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }
}