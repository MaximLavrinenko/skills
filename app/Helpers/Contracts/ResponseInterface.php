<?php

namespace App\Helpers\Contracts;

interface ResponseInterface
{
    const FAILURE = 0;

    const SUCCESS = 1;

    public function isSuccessful(): bool;

    public function setCode(int $code);

    public function getMessages(): array;

    public function addMessage(string $message);

    public function getData();

    public function setData($data);
}