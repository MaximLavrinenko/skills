<?php

namespace App\Http\Controllers;

use App\Models\Projects;
use Illuminate\Support\Facades\Input;
use App\Service\UserService;
use App\Service\PermissionService;

class Skills extends Controller
{
    /**
     * Transfer of arrays with all projects and user projects in view
     *
     * @param UserService $service
     * @param PermissionService $permissionService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(UserService $service, PermissionService $permissionService)
    {
        if ($this->requestUser() === null) {

            return view('404');
        }

        if($permissionService->canViewUserData($this->requestUser())){

            $user = $this->requestUser();
            $myProjects = $service->getUserProjectIds($this->requestUser());
            $allProjects = Projects::all()->toArray();
            $myStatus = $user->status;

            return view(
                'skills.skills',
                [
                    'js' => [
                        'myProjects' => $myProjects,
                        'allProjects' => $allProjects,
                        'myStatus' => $myStatus,
                        'user' => $user,
                    ]
                ]
            );
        }

        return view('403');
    }

    /**
     * Adding projects to the user in "My projects"
     *
     * @param UserService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeUserProjects(UserService $service)
    {
        $checkedProjects = Input::get('checkedProjects');

        $response = $service->changeUserProjects($checkedProjects, $this->requestUser());

        return $this->composeResponse($response);
    }

    /**
     * Delete projects by the user from "My projects"
     *
     * @param UserService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUserProjects(UserService $service)
    {
        $projectId = Input::get('projectId');

        $response = $service->deleteUserProjects($projectId, $this->requestUser());

        return $this->composeResponse($response);
    }

    /**
     * Update user status
     *
     * @param UserService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserStatus(UserService $service)
    {
        $value = Input::get('value');

        $response = $service->updateUserStatus($value, $this->requestUser());

        return $this->composeResponse($response);
    }
}