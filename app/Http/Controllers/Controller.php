<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Helpers\Contracts\ResponseInterface;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Auth;
use App\Helpers\CustomResponse;
use Mockery\Exception;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Correcting for a parameter - user_id
     *
     * @return User
     */
    protected function requestUser()
    {
        $id = request('user_id');

        if ($id === null) {

            return Auth::user();
        }

        return User::find((int)$id);
    }

    /**
     * Additional data for the response
     *
     * @param ResponseInterface $response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function composeResponse(ResponseInterface $response)
    {
        $payload = [
            'is_successful' => $response->isSuccessful(),
            'has_error' => !$response->isSuccessful() && count($response->getMessages()),
            'messages' => $response->getMessages(),
            'data' => $response->getData(),
        ];

        return response()->json($payload);
    }
}
