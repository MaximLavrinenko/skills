<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersProjects extends Model
{
    protected $table = 'users_projects';

    public function projects()
    {
        return $this->belongsToMany('App\Models\Projects', 'project_id');
    }
}
