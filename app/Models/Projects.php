<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;

class Projects extends Model
{

    /**
     * Output of the projects of an authorized user
     *
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function usersProjects(User $user)
    {
        $query = static::query();
        $query
            ->select('projects.*')
            ->join('users_projects', 'users_projects.project_id', '=', 'projects.id')
            ->where('users_projects.user_id', $user->id)
            ->groupBy('projects.id');

        return $query;
    }

    /**
     * Many-to-many relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user()
    {
        return $this->belongsToMany('App\Models\Auth\User', 'users_projects', 'project_id', 'user_id');
    }
}
