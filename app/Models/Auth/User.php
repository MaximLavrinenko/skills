<?php

namespace App\Models\Auth;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\Auth;

/**
 * Class User
 * @package App\Models\Auth
 */
class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    const STATUS_IDLE = 'Idle';
    const STATUS_PART_TIME_OCCUPIED = 'Part time occupied';
    const STATUS_FULL_TIME_OCCUPIED = 'Full time occupied';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'is_active'
    ];

    /**
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * @return string
     */
    public function getFullName() : string
    {
        return $this->last_name . ' ' . $this->name;
    }

    /**
     * disable cache
     * @return mixed
     */
    public function cachedRoles()
    {
        return $this->roles()->get();
    }

    /**
     * @return array
     */
    public static function  availableUserStatuses()
    {
        return [static::STATUS_IDLE, static::STATUS_PART_TIME_OCCUPIED, static::STATUS_FULL_TIME_OCCUPIED,];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany('App\Models\Projects', 'users_projects', 'user_id', 'project_id');
    }

    /**
     * Receiving and verifying the user's role
     *
     * @return bool
     */
    public function isAdmin():bool
    {
        return $this->hasRole([Role::ROLE_SUPERADMIN, Role::ROLE_ADMIN]);
    }
}
