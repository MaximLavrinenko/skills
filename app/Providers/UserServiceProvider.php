<?php

namespace App\Providers;

use App\Service\UserService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Service\PermissionService;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserService::class, function($app){
            return new UserService($app->make(PermissionService::class));
        });
    }
}
