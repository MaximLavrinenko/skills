<?php

namespace App\Service;

use App\Models\Auth\User;
use App\Helpers\CustomResponse;

class UserService
{
    private $permissionService;

    function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * Creating an array with user projects
     *
     * @param User $user
     * @return \Illuminate\Support\Collection
     */
    public function getUserProjectIds(User $user)
    {
        return $user->projects()->pluck('id');
    }

    /**
     * Update user status
     *
     * @param $userStatus
     * @param User $user
     * @return CustomResponse
     */
    public function updateUserStatus($userStatus, User $user)
    {
        $response = new CustomResponse();

        if($this->permissionService->canUpdateUserData($user)) {

            if (!in_array($userStatus, User::availableUserStatuses())) {

                $response->setCode(CustomResponse::FAILURE)
                    ->addMessage('Error. Unable to update status. Invalid status value has been provided.')
                    ->setData('');

                return $response;
            }

            $user->status = $userStatus;
            $user->save();

            $response->setCode(CustomResponse::SUCCESS)
                ->addMessage('Success. Status has been successfully updated.')
                ->setData($user->status);

            return $response;
        }

        $response->setCode(CustomResponse::FAILURE)
            ->addMessage('Error. Permission Denied. Status not changed.')
            ->setData('');

        return $response;
    }

    /**
     * Deleted user project
     *
     * @param $projectId
     * @param User $user
     * @return CustomResponse
     */
    public function deleteUserProjects($projectId,  User $user)
    {
        $response = new CustomResponse();

        if($this->permissionService->canUpdateUserData($user)) {

            if($user->projects()->detach($projectId)) {

                $response->setCode(CustomResponse::SUCCESS)
                    ->addMessage('Success. Project successfully deleted.')
                    ->setData($user->projects()->pluck('id'));

                return $response;
            }

            $response->setCode(CustomResponse::FAILURE)
                ->addMessage('Error. Unable to delete project.')
                ->setData($user->projects()->pluck('id'));

            return $response;
        }

        $response->setCode(CustomResponse::FAILURE)
            ->addMessage('Error. Permission Denied. Project not deleted.')
            ->setData($user->projects()->pluck('id'));

        return $response;
    }

    /**
     * Adding projects to the user in "My projects"
     *
     * @param $checkedProjects
     * @param User $user
     * @return CustomResponse
     */
    public function changeUserProjects($checkedProjects, User $user)
    {
        $response = new CustomResponse();

        if($this->permissionService->canUpdateUserData($user)) {

            if(empty($checkedProjects)) {

                $response->setCode(CustomResponse::FAILURE)
                    ->addMessage('Error. Project is not selected.')
                    ->setData('');

                return $response;
            }

            foreach ($checkedProjects as $projectId) {
                $user->projects()->attach($projectId, ['active' => 1]);
            }

            $response->setCode(CustomResponse::SUCCESS)
                ->addMessage('Success. Project added.')
                ->setData($user->projects()->pluck('id'));

            return $response;
        }

        $response->setCode(CustomResponse::FAILURE)
            ->addMessage('Error. Permission Denied. Project not added.')
            ->setData('');

        return $response;
    }
}