<?php

namespace App\Service;

use App\Models\Auth\User;

class PermissionService
{
    private $currentUser;

    /**
     * PermissionService constructor.
     *
     * @param User $currentUser
     */
    public function __construct(User $currentUser = null)
    {
        if(is_null($currentUser)) {

            return $this->currentUser = new User();
        }

        return $this->currentUser = $currentUser;
    }

    /**
     * Check for the ability to edit user data
     *
     * @param User $forUser
     * @return bool
     */
    public function canUpdateUserData(User $forUser): bool
    {
        return $this->currentUser->id === $forUser->id || $this->currentUser->isAdmin();
    }

    /**
     * Check for the ability to see user data
     *
     * @param User $forUser
     * @return bool
     */
    public function canViewUserData(User $forUser): bool
    {
        return $this->canUpdateUserData($forUser);
    }
}