<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['jwt']], function () {

    Route::group(['middleware' => ['auth', 'check_user', 'refresh_jwt']], function () {

        Route::get('/', 'Skills@index')->name('main');

        Route::post('/user-projects/change', 'Skills@changeUserProjects');
        Route::delete('/user-projects/delete', 'Skills@deleteUserProjects');
        Route::post('/user-status/status', 'Skills@updateUserStatus');

    });

});

