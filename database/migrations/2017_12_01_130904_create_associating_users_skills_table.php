<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociatingUsersSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create table for associating skills to users (Many-to-Many)
        Schema::create('users_skills', function (Blueprint $table) {
            $table->integer('users_id')->unsigned();
            $table->integer('skills_id')->unsigned();
            $table->integer('proficiency_id')->unsigned();

            $table->foreign('users_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('skills_id')->references('id')->on('skills')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('proficiency_id')->references('id')->on('proficiency')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['users_id', 'skills_id', 'proficiency_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_skills');
    }
}
