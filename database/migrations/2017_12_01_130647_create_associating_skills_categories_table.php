<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociatingSkillsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create a spreadsheet for a skill bundle with categories (Many-to-Many)
        Schema::create('skills_categories', function (Blueprint $table) {
            $table->integer('skills_id')->unsigned();
            $table->integer('categories_id')->unsigned();

            $table->foreign('skills_id')->references('id')->on('skills')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('categories_id')->references('id')->on('categories')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['skills_id', 'categories_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('skills_categories');
    }
}
